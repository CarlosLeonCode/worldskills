import React from 'react';
import './styles/App.css';

// components 
import Nav from './components/Navbar';
import Wall from './components/ProfileWall';
import Presentation from './components/Presentation';
import Phrase from './components/Phrase';
import Skills from './components/Skills'
import Contact from './components/Contact'
// Data 
import dataSkills from './data/skills.json'

function App() {
  return (
    <div className="App">
      {/* Start: navbar  */}
      <Nav />

      {/* Start: Wall */}
      <Wall name="Carlos Esteban León Pinilla" profession="Software developer - Full stack" />

      {/* Start: Presentation */}
      <Presentation />
      <hr className="hr short" />  

      {/* Start: Phrase */}
      <Phrase />
      <hr className="hr long" />  

      {/* Start: Skills */}
      <Skills skills={dataSkills} />
      <hr className="hr long" />  

      {/* Start: Skills */}
      <Contact />

    </div>
  );
}

export default App;
