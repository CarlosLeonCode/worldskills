import React from 'react';
import '../styles/photo.css'
import image from '../img/photo.jpg'

export default function Photo(){
    return(
        <img src={image} alt="profile" className="photo-profle" />
    )
}