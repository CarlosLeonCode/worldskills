import React from 'react';
// style
import '../styles/skillsCard.css';

export default function SkillCard(props){

    function showDetails(event){
        console.log('temp')
    }

    return(
        <div className="container-card">
            <div className="card" onClick={showDetails.bind(this)}>
                <div className="card-body">
                    <i className={props.data.icon}></i>
                </div>
                <div className="card-footer">
                    <p>{props.data.name}</p>  
                </div>
                <div className="details">
                    <p className="text">{props.data.Description}</p>
                </div>
            </div>
        </div>
    )
}

