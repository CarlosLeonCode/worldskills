import React from 'react';
import '../styles/navbar.css';

export default function Navbar(){
    return(
        <nav className="navbar-app container">
            
            <div className="nav-mobile">
                {/* Icon code  */}
                <i className="fas fa-terminal code-icon"></i>  
                {/* Icon phone */}
                <i className="fas fa-phone phone-icon"></i>
                {/* Icon brain  */}
                <i className="fas fa-brain brain-icon"></i>
                {/* Icon home*/}
                <i className="fas fa-home home-icon"></i>
            </div>

            <ul className="nav-desktop">
                <li className="code-icon"><i className="fas fa-terminal code-icon"></i>  </li>
                <li className="contact-text box-item">Contact</li>
                <li className="skills-text box-item">Skills</li>
                <li className="home-text box-item">Home</li>
            </ul>
        </nav>
    )
}