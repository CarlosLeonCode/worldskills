import React from 'react';
import '../styles/presentation.css';

export default function Presentation(){
    return(
        <div className="container presentation">
            <div className="content">
                <h3 className="title">¿Quién soy yo?</h3>
                <p className="text">
                    Un chico apasionado por la tecnología, la programación y la música.
                </p>
            </div>
        </div>
    )
}