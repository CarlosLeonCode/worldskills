import React from 'react';
// styles 
import '../styles/skills.css';
// components 
import SkillCard from './SkillCard';


export default function Skills(props){
    return(
        <div className="container skills">
            <div className="content">
                <div className="head">
                    <h3 className="title">Skills</h3>
                </div>
                <div className="body">
                    {
                        props.skills.map((skill) => <SkillCard data={skill} key={skill.id} />)
                    }
                </div>
            </div>
        </div>
    )
}