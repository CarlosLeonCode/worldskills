import React from 'react';
// Style 
import '../styles/contact.css';
// Image 
import Image from '../img/hologram.png';
import Phone from '../img/telefono.png';
import Email from '../img/gmail.png';


export default function Contact(){
    return(
        <div className="container">
            <div className="content contact">
                <h3 className="title">Contact</h3>
                <div className="image">
                    <img src={Image} alt="phone contact" />
                </div>
                <div className="social-media">
                    <div className="phone-container">
                        <img src={Phone} alt="telephone" />
                        <span>3194588796</span>
                    </div>
                    <div className="email-container">
                        <img src={Email} alt="email" />
                        <span>Carlos@gmail.com</span>
                    </div>
                    <hr className="hr short"/>
                    <div className="social-icons">
                        <i className="fab fa-twitter-square"></i>
                        <i className="fab fa-facebook-square"></i>
                        <i className="fab fa-instagram"></i>
                    </div>
                </div>
            </div>
        </div>
    )
}