import React from 'react';
import '../styles/phrase.css';
// Image 
import Boy from '../img/programmer.png';

export default function Phrase(){
    return(
        <div className="container phrase">
            <div className="content">
                <p className="text">
                    ¡Aprende, práctica, corrige, escucha una  buena canción y nunca te rindas!. 
                </p>
                <img src={Boy} alt="programmer" className="img-boy"/>
            </div>
        </div>
    )   
}