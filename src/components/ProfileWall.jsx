import React from 'react';
import '../styles/profile-wall.css';


// Components 
import Photo from './Photo'

export default function ProfileWall(props){

    const { name, profession } = props;


    return(
        <div className="wall">
            <div className="container content-wall">
                <Photo />
                <div className="container-profile-details">
                    <div className="content">
                        <span className="name">{name}</span>
                        <span className="profession">{profession}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}